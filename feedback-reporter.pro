QT += core widgets

HEADERS += \
    src/feedbackreporterapp.hh \
    src/feedbackreportwindow.hh

SOURCES += \
    src/feedbackreporterapp.cc \
    main.cc \
    src/feedbackreportwindow.cc

RESOURCES += \
    rsrc/feedback-reporter.qrc
