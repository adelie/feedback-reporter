==============================
 README for Feedback Reporter
==============================
:Authors:
  * **A. Wilcox**, primary developer
:Status:
  Beta
:Copyright:
  © 2017 Adélie Linux Team.  NCSA open source licence.




Introduction
============

This repository contains code and documentation for the Adélie Linux Feedback
Reporter, a small Qt5-based utility designed to allow users easily report
issues or comments about the current operating environment.

This utility is designed for easily collecting information from users and
submitting it to an XML endpoint.  Combined with the feedback-reporter-bz
daemon (or a similar service) installed on a server, this provides a way for
users to have direct input to an internal tracking system.

In the Adélie Linux project, this system is used during alpha and beta testing,
allowing users to provide quick bug reports or package requests.

This utility may be useful to other distributions to gather user issues, or in
corporate environments to allow users to file support tickets.  While other
users are welcome, please remember as noted in the license that this software
is provided to you "AS IS" without any warranty, expressed or implied.


License
```````

As the Adélie Linux project is an open-source Linux distribution, this package
is distributed under the same NCSA open source license as the distribution.


Changes
```````

If you have an improvement or fix for feedback-reporter, please send it to the
appropriate Adélie Linux mailing list or open a pull request on our GitLab.

For more information on contributing your changes, see the CONTRIBUTING.rst
file at the root of this repository.




Configuration
=============

This section contains information about configuring the Feedback Reporter.
Most configuration takes place at build-time.  Some values are overridable in
the environment, if environment variable configuration is enabled.  Note that
depending on your environment, this may present a security risk; therefore,
environment variable configuration is disabled by default.


XML Endpoint
````````````

The default XML endpoint URI will be gathered during build time.

If environment-based configuration is enabled, the software will inspect the
FEEDBACK_XML environment variable.  If set and non-empty, this will be used
instead of the built-in XML endpoint URI.


Categories
``````````

The default category list is in the resource collection under categories.txt.

If environment-based configuration is enabled, the software will inspect the
FEEDBACK_CATEGORIES_LIST environment variable.  If set to a valid, non-empty,
readable file, each line of the specified file will be shown as a category in
the following format:

XML\tUser readable name

If no \t is found on a line, the entire line is sent in the XML.

A "#" at the beginning of a line denotes a comment.  Empty lines are ignored.
