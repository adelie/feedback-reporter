#ifndef FEEDBACKREPORTERAPP_HH
#define FEEDBACKREPORTERAPP_HH

#include <QApplication>
#include <QComboBox>
#include <QStringList>
#include <QSystemTrayIcon>

class FeedbackReporterApp : public QApplication
{
public:
        FeedbackReporterApp(int argc, char *argv[]);
        void addCategories(QComboBox *box);

private:
        QStringList items;
        QStringList raw;
        QSystemTrayIcon *trayIcon;
};

#endif // FEEDBACKREPORTERAPP_HH
