#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>

#include "feedbackreporterapp.hh"
#include "feedbackreportwindow.hh"

FeedbackReportWindow::FeedbackReportWindow(QWidget *parent) : QMainWindow(parent)
{
        QWidget *centralWidget = new QWidget(this);
        QVBoxLayout *myLayout = new QVBoxLayout(centralWidget);
        QHBoxLayout *buttonLayout = new QHBoxLayout(centralWidget);


        QLabel *categoryLabel = new QLabel(tr("&Category:"), this);
        category = new QComboBox(centralWidget);
        ((FeedbackReporterApp *)qApp)->addCategories(category);
        categoryLabel->setBuddy(category);

        myLayout->addWidget(categoryLabel);
        myLayout->addWidget(category);
        myLayout->addSpacing(categoryLabel->height());


        /*QLabel *subjectLabel = new QLabel(tr("&Subject:"), this);
        subject = new QLineEdit(this);
        subject->setPlaceholderText(tr("General comment about Adélie Linux"));
        subject->setToolTip(tr("Write a short summary of your feedback."));
        subjectLabel->setBuddy(subject);

        myLayout->addWidget(subjectLabel);
        myLayout->addWidget(subject);
        myLayout->addSpacing(subjectLabel->height());*/


        QLabel *bodyLabel = new QLabel(tr("&Feedback:"), this);
        body = new QPlainTextEdit(this);
        body->setToolTip(tr("Write your feedback here."));
        bodyLabel->setBuddy(body);

        myLayout->addWidget(bodyLabel);
        myLayout->addWidget(body);


        send = new QPushButton(QIcon::fromTheme("mail-send-symbolic"),
                               tr("Send"), centralWidget);
        cancel = new QPushButton(QIcon::fromTheme("dialog-cancel"),
                                 tr("Don't Send"), centralWidget);
        buttonLayout->addWidget(cancel);
        buttonLayout->addStretch();
        buttonLayout->addWidget(send);
        myLayout->addLayout(buttonLayout);


        QObject::connect(cancel, &QPushButton::clicked,
                         [=]() {
                this->close();
                this->deleteLater();
        });
        this->setCentralWidget(centralWidget);
}
