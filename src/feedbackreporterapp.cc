#include "feedbackreporterapp.hh"
#include "feedbackreportwindow.hh"

#include <QMenu>
#include <QFile>
#include <QTextStream>

FeedbackReporterApp::FeedbackReporterApp(int argc, char *argv[])
        : QApplication(argc, argv)
{
        this->setApplicationDisplayName(tr("Feedback Reporter"));
        this->setApplicationName(tr("Adélie Feedback Reporter"));
        this->setApplicationVersion("1.0-alpha3");
        this->trayIcon = new QSystemTrayIcon(QIcon::fromTheme("comment-symbolic"), this);

        QMenu *menu = new QMenu;
        menu->addAction(QIcon::fromTheme("application-exit-symbolic"),
                        tr("Quit"), &QApplication::quit,
                        QKeySequence::Quit);
        this->trayIcon->setContextMenu(menu);

        QObject::connect(this->trayIcon, &QSystemTrayIcon::activated,
                         [=](QSystemTrayIcon::ActivationReason reason) {
                FeedbackReportWindow *window;
                switch(reason)
                {
                case QSystemTrayIcon::Trigger:
                        window = new FeedbackReportWindow();
                        window->show();
                        break;
                default:
                        break;
                }
        });
        this->trayIcon->show();

        // Initialise category lists.
        QFile categoryFile(":/categories.txt");
        if(!categoryFile.open(QFile::ReadOnly))
        {
                items << tr("Other");
                raw << tr("Other");
        }
        else
        {
                QTextStream myStream(&categoryFile);
                QString line;
                while(myStream.readLineInto(&line))
                {
                        if(line.isEmpty() || line.at(0) == '#')
                        {
                                // Blank lines and comments are ignored
                                continue;
                        }

                        if(line.contains('\t'))
                        {
                                raw << line.section('\t', 0, 0);
                                items << line.section('\t', 1);
                        }
                        else
                        {
                                // No tab character - use the whole line
                                items << line;
                                raw << line;
                        }
                }
        }
}

void FeedbackReporterApp::addCategories(QComboBox *box)
{
        size_t index;

        for(index = 0; index < items.length(); index++)
        {
                box->addItem(items.at(index), raw.at(index));
        }
}
