#ifndef FEEDBACKREPORTWINDOW_HH
#define FEEDBACKREPORTWINDOW_HH

#include <QMainWindow>
#include <QComboBox>
//#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>

class FeedbackReportWindow : public QMainWindow
{
        Q_OBJECT
public:
        explicit FeedbackReportWindow(QWidget *parent = 0);

signals:

public slots:

private:
        QComboBox *category;
        //QLineEdit *subject;
        QPlainTextEdit *body;
        QPushButton *send;
        QPushButton *cancel;
};

#endif // FEEDBACKREPORTWINDOW_HH
