#include <src/feedbackreporterapp.hh>
#include <QLibraryInfo>
#include <QTranslator>

int main(int argc, char *argv[])
{
        FeedbackReporterApp app(argc, argv);

        QString translatorFileName = QLatin1String("qt_");
        translatorFileName += QLocale::system().name();
        QTranslator *translator = new QTranslator(&app);
        if(translator->load(translatorFileName,
                            QLibraryInfo::location(
                                    QLibraryInfo::TranslationsPath
                            )))
        {
                app.installTranslator(translator);
        }

        return app.exec();
}
