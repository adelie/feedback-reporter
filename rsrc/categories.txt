# Describes available categories for the Feedback Reporter
# English (Standard)

missingpkg	I can't find a program that I want to use
bug	A program is behaving incorrectly or closing when I use it
upstream	I'm having a problem trying to use a program
design	I have an idea to make Adélie better
other	The feedback I want to provide isn't listed here
